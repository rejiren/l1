from django.conf.urls import patterns, include, url
from demo import settings


urlpatterns = patterns('',
    url(r'^', include('core.urls')),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)