from django import forms
from django.utils.dates import MONTHS

class Filter(forms.Form):
    month = forms.ChoiceField(
        choices=MONTHS.items(),
        widget=forms.widgets.Select(
            attrs={'class':"form-control",
                   'placeholder':'Month'})
    )