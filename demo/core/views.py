import datetime

from django.utils import simplejson as json
from django.db.models import Count, Q
from django.http import HttpResponse
from django.template.context import Context
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, FormView

from .constants import get_chart_data, Chart
from .forms import Filter
from .models import Incidents, Problems


class ChartView(TemplateView):
    template_name = 'front/base.html'
    filter_form = None
    x_scale = None
    y_scale = None
    x_scale_type=None,
    y_scale_type=None,
    type=None

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ChartView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        print request.POST
        data = {
            'state': 123,
            'chart_data': self.chart_data(*args, **kwargs),
            'filter_form': self.filter_form_html
        }
        if 'filter' in request.POST:
            data['chart_data'] = self.chart_data(filter=request.POST['filter'])
        return HttpResponse(json.dumps(data))

    def chart_data(self, *args, **kwargs):
        chart = Chart(
            objects=self.objects(*args, **kwargs),
            x_scale=self.x_scale,
            y_scale=self.y_scale,
            x_scale_type=self.x_scale_type,
            y_scale_type=self.y_scale_type,
            type=self.type
        )
        return chart.data

    @property
    def chart_json_data(self):
        return json.dumps(self.chart_data())

    @property
    def filter_form_html(self):
        if self.filter_form:
            t = get_template('dynamic/chart_filter.html')
            html = t.render(Context({'form': self.filter_form}))
            return html
        return None


class ChartOneView(ChartView):
    def objects(self, *args, **kwargs):
        incidents = Incidents.objects.filter(server="null").\
            extra(select={'month': "EXTRACT(month FROM date)"}).\
            values("month").\
            annotate(Count("date"))
        incidents_with_server = Incidents.objects.exclude(server="null").\
            extra(select={'month': "EXTRACT(month FROM date)"}).\
            values("month").\
            annotate(Count("date"))
        self.x_scale = [obj['month'] for obj in incidents] + [obj['month'] for obj in incidents_with_server]
        item = {}
        for i in incidents:
            item[i['month']] = i['date__count']
        item2 = {}
        for i in incidents_with_server:
            item2[i['month']] = i['date__count']
        return [item, item2]


class ChartTwoView(ChartView):
    filter_form = Filter
    def objects(self, *args, **kwargs):
        month = kwargs['filter'] if 'filter' in kwargs else datetime.datetime.now().month
        incidents = Incidents.objects.values("server").annotate(Count("server"))
        item = {}
        for i in incidents:
            if i['server__count'] > 50 and i['server'] != 'null':
                item[i['server']] = i['server__count']
        self.x_scale = sorted(item.keys(), key=lambda x:item[x], reverse=True)
        return [item]


class ChartThreeView(ChartView):
    def objects(self):
        problems = Problems.objects.values_list("pattern", flat=True)
        objects = []
        for problem in problems:
            incidents = Incidents.objects.filter(summary__contains=problem).\
                extra(select={'month': "EXTRACT(month FROM date)"}).\
                values("month").\
                annotate(Count("date"))
            self.x_scale = [obj['month'] for obj in incidents]
            item = {}
            for i in incidents:
                item[i['month']] = i['date__count']
            objects.append(item)
        return objects


class ChartFourView(ChartView):
    def objects(self):
        problems = Problems.objects.values_list("pattern", flat=True)
        item = {}
        for problem in problems:
            incidents_count = Incidents.objects.filter(summary__contains=problem).count()
            self.x_scale = problems
            item[problem] = incidents_count
        return [item]

class GridView(TemplateView):
    template_name = 'front/table.html'

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(GridView, self).dispatch(*args, **kwargs)

    @property
    def grid_html(self):
        t = get_template('dynamic/table.html')
        html = t.render(Context({'objects': self.objects}))
        return html

    @property
    def objects(self):
        return Incidents.objects.all()[:30]

grid_view = GridView.as_view()
chart_one_view = ChartOneView.as_view()
chart_two_view = ChartTwoView.as_view()
chart_three_view = ChartThreeView.as_view()
chart_four_view = ChartFourView.as_view()
