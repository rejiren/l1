# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class ClustersList(models.Model):
    cluster = models.IntegerField()
    realm = models.IntegerField()
    bwc = models.TextField()
    class Meta:
        managed = False
        db_table = 'clusters_list'

class ClustersListFull(models.Model):
    bwc = models.TextField()
    cluster = models.TextField()
    realm = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'clusters_list_full'

class ClustersScheme(models.Model):
    id = models.IntegerField(primary_key=True)
    realm = models.IntegerField()
    name = models.TextField()
    bwc = models.TextField()
    sort_order = models.IntegerField()
    enabled = models.IntegerField()
    production = models.IntegerField()
    date_start = models.DateTimeField()
    date_end = models.DateTimeField(blank=True, null=True)
    id_prev = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'clusters_scheme'

class Hosts(models.Model):
    id = models.IntegerField(primary_key=True)
    realm = models.IntegerField()
    cluster = models.IntegerField()
    project = models.IntegerField()
    hostname = models.TextField()
    role = models.TextField()
    details = models.TextField()
    comments = models.TextField()
    responsible = models.TextField()
    status = models.TextField()
    mark = models.IntegerField()
    location = models.TextField()
    s_n = models.TextField(db_column='s/n') # Field renamed to remove unsuitable characters.
    mgmt_ip = models.TextField(db_column='mgmt ip') # Field renamed to remove unsuitable characters.
    be_ip = models.TextField(db_column='be ip') # Field renamed to remove unsuitable characters.
    fe_ip = models.TextField(db_column='fe ip') # Field renamed to remove unsuitable characters.
    mgmt_vlan = models.TextField(db_column='mgmt vlan') # Field renamed to remove unsuitable characters.
    be_vlan = models.TextField(db_column='be vlan') # Field renamed to remove unsuitable characters.
    fe_vlan = models.TextField(db_column='fe vlan') # Field renamed to remove unsuitable characters.
    tasks = models.TextField()
    hw_details = models.TextField(db_column='hw details') # Field renamed to remove unsuitable characters.
    ilo_mac = models.TextField(db_column='ilo mac') # Field renamed to remove unsuitable characters.
    pxe_mac = models.TextField(db_column='pxe mac') # Field renamed to remove unsuitable characters.
    pass_field = models.TextField(db_column='pass') # Field renamed because it was a Python reserved word.
    date_from = models.DateTimeField()
    date_to = models.DateTimeField(blank=True, null=True)
    id_prev = models.IntegerField(blank=True, null=True)
    dc = models.TextField()
    type = models.TextField()
    class Meta:
        managed = False
        db_table = 'hosts'

class Hosts2(models.Model):
    id = models.IntegerField(primary_key=True)
    realm = models.IntegerField()
    cluster = models.IntegerField()
    project = models.IntegerField()
    hostname = models.TextField()
    role = models.TextField()
    details = models.TextField()
    comments = models.TextField()
    responsible = models.TextField()
    status = models.TextField()
    mark = models.IntegerField()
    location = models.TextField()
    s_n = models.TextField(db_column='s/n') # Field renamed to remove unsuitable characters.
    mgmt_ip = models.TextField(db_column='mgmt ip') # Field renamed to remove unsuitable characters.
    be_ip = models.TextField(db_column='be ip') # Field renamed to remove unsuitable characters.
    fe_ip = models.TextField(db_column='fe ip') # Field renamed to remove unsuitable characters.
    mgmt_vlan = models.TextField(db_column='mgmt vlan') # Field renamed to remove unsuitable characters.
    be_vlan = models.TextField(db_column='be vlan') # Field renamed to remove unsuitable characters.
    fe_vlan = models.TextField(db_column='fe vlan') # Field renamed to remove unsuitable characters.
    tasks = models.TextField()
    hw_details = models.TextField(db_column='hw details') # Field renamed to remove unsuitable characters.
    ilo_mac = models.TextField(db_column='ilo mac') # Field renamed to remove unsuitable characters.
    pxe_mac = models.TextField(db_column='pxe mac') # Field renamed to remove unsuitable characters.
    pass_field = models.TextField(db_column='pass') # Field renamed because it was a Python reserved word.
    date_from = models.DateTimeField()
    date_to = models.DateTimeField(blank=True, null=True)
    id_prev = models.IntegerField(blank=True, null=True)
    dc = models.TextField()
    type = models.TextField()
    class Meta:
        managed = False
        db_table = 'hosts_2'

class HostsList(models.Model):
    id = models.IntegerField()
    realm = models.TextField(blank=True)
    cluster = models.TextField(blank=True)
    server = models.TextField()
    role = models.TextField()
    details = models.TextField()
    class Meta:
        managed = False
        db_table = 'hosts_list'

class HostsRev(models.Model):
    id = models.IntegerField(primary_key=True)
    realm = models.IntegerField()
    cluster = models.IntegerField()
    project = models.IntegerField()
    hostname = models.TextField()
    role = models.TextField()
    details = models.TextField()
    comments = models.TextField()
    responsible = models.TextField()
    status = models.TextField()
    mark = models.IntegerField()
    location = models.TextField()
    s_n = models.TextField(db_column='s/n') # Field renamed to remove unsuitable characters.
    mgmt_ip = models.TextField(db_column='mgmt ip') # Field renamed to remove unsuitable characters.
    be_ip = models.TextField(db_column='be ip') # Field renamed to remove unsuitable characters.
    fe_ip = models.TextField(db_column='fe ip') # Field renamed to remove unsuitable characters.
    mgmt_vlan = models.TextField(db_column='mgmt vlan') # Field renamed to remove unsuitable characters.
    be_vlan = models.TextField(db_column='be vlan') # Field renamed to remove unsuitable characters.
    fe_vlan = models.TextField(db_column='fe vlan') # Field renamed to remove unsuitable characters.
    tasks = models.TextField()
    hw_details = models.TextField(db_column='hw details') # Field renamed to remove unsuitable characters.
    ilo_mac = models.TextField(db_column='ilo mac') # Field renamed to remove unsuitable characters.
    pxe_mac = models.TextField(db_column='pxe mac') # Field renamed to remove unsuitable characters.
    pass_field = models.TextField(db_column='pass') # Field renamed because it was a Python reserved word.
    date_from = models.DateTimeField()
    date_to = models.DateTimeField(blank=True, null=True)
    id_prev = models.IntegerField(blank=True, null=True)
    dc = models.TextField()
    type = models.TextField()
    class Meta:
        managed = False
        db_table = 'hosts_rev'

class Incidents(models.Model):
    id = models.IntegerField(primary_key=True)
    server = models.TextField()
    key = models.TextField()
    date = models.DateTimeField()
    summary = models.TextField()
    type = models.TextField()
    class Meta:
        managed = False
        db_table = 'incidents'

class IncidentsAll(models.Model):
    id = models.IntegerField()
    server = models.TextField()
    key = models.TextField()
    date = models.DateTimeField()
    summary = models.TextField()
    cluster = models.TextField(blank=True)
    realm = models.TextField(blank=True)
    role = models.TextField(blank=True)
    details = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'incidents_all'

class Problems(models.Model):
    id = models.IntegerField(primary_key=True)
    pattern = models.TextField()
    type = models.TextField()
    class Meta:
        managed = False
        db_table = 'problems'

class Projects(models.Model):
    id = models.IntegerField(primary_key=True)
    project = models.TextField()
    class Meta:
        managed = False
        db_table = 'projects'

class Realms(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField()
    project = models.IntegerField()
    sort_order = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'realms'

class ServersWarrantyEnd(models.Model):
    id = models.IntegerField()
    server = models.TextField()
    sn = models.TextField()
    dc = models.TextField()
    date = models.DateField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'servers_warranty_end'

class ServersWithoutWarranty(models.Model):
    id = models.IntegerField()
    server = models.TextField()
    sn = models.TextField()
    dc = models.TextField()
    hw_details = models.TextField(db_column='hw details') # Field renamed to remove unsuitable characters.
    class Meta:
        managed = False
        db_table = 'servers_without_warranty'

class Warranty(models.Model):
    id = models.IntegerField(primary_key=True)
    sn = models.TextField()
    pn = models.TextField()
    desc = models.TextField()
    check_date = models.DateTimeField()
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'warranty'

