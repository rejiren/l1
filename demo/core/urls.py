from django.conf.urls import patterns, include, url

from core import views

urlpatterns = patterns('',
    url(r'^$', views.grid_view, name='grid'),
    url(r'^1/$', views.chart_one_view, name='1'),
    url(r'^2/$', views.chart_two_view, name='2'),
    url(r'^3/$', views.chart_three_view, name='3'),
    url(r'^4/$', views.chart_four_view, name='4'),
)