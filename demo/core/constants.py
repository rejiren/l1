__author__ = 'ren'
from django.utils import simplejson as json

CHART_OBJECT = {
              "className": ".pizza",
              "data": [
                {
                  "x": "Pepperoni2",
                  "y": 12
                },
                {
                  "x": "Cheese",
                  "y": 8
                }
              ]
            }

CHART_OBJECT2 = {
              "className": ".pizza2",
              "data": [
                {
                  "x": "Pepperoni",
                  "y": 10
                },
                {
                  "x": "Cheese",
                  "y": 11
                }
              ]
            }

CHART_DATA = {
          "xScale": "ordinal", #ordinal linear time exponential
          "yScale": "linear", #ordinal linear time exponential
          "type": "line-dotted", #bar cumulative line line-dotted
          "main": [CHART_OBJECT, CHART_OBJECT2]
          }
tmp_list = [{
            "Pepperoni2": 10,
            "Pepperoni": 23,
            "Cheese": 12,
            },
            {
            "Pepperoni2": 16,
            "Pepperoni": 11,
            "Cheese": 9,
            },
            ]

SCALE_TYPES = {

}


class Chart():
    def __init__(self, objects, x_scale=None, y_scale=None, x_scale_type=None, y_scale_type=None, type=None):
        self.objects = objects
        self.x_scale = x_scale
        self.y_scale = y_scale
        self.x_scale_type = x_scale_type
        self.y_scale_type = y_scale_type
        self.type = type or 'bar'

    def get_object_data(self, obj_data, prefix):
        data = {
            "className": prefix,
            "data": [
                {
                    'y': obj_data[key],
                    'x': key
                } for key in self.x_scale if key in obj_data.keys()
            ],
        }
        return data

    @property
    def data(self):
        data = {
            "xScale": "ordinal", #ordinal linear time exponential
            "yScale": "linear", #ordinal linear time exponential
            "type": self.type, #bar cumulative line line-dotted
            "main": [self.get_object_data(item, index) for index, item in enumerate(self.objects)]
        }
        return data


def get_scale_x():
    return ["Pepperoni2", "Pepperoni", "Cheese"]


def get_chart_object(obj_data, scale_x, prefix=None):
    data = {
            "className": prefix,
            "data": [{'y': obj_data[key], 'x': key} for key in scale_x],
            }
    return data

def get_chart_data(obj_list=tmp_list, scale_x=get_scale_x()):
    data = {
            "xScale": "ordinal", #ordinal linear time exponential
            "yScale": "linear", #ordinal linear time exponential
            "type": "line-dotted", #bar cumulative line line-dotted
            "main": [get_chart_object(item, scale_x, index) for index, item in enumerate(obj_list)]
            }
    print data
    return json.dumps(data)

'''
            {
              "className": ".pizza2",
              "data": [
                {
                  "x": "Pepperoni",
                  "y": 11
                },
                {
                  "x": "Cheese",
                  "y": 10
                }
              ]
            },
          ],
        }
'''
'''
          "comp": [
            {
              "className": ".pizza",
              "type": "line-dotted",
              "data": [
                {
                  "x": "Pepperoni",
                  "y": 10
                },
                {
                  "x": "Cheese",
                  "y": 4
                }
              ]
            }
          ]
          '''